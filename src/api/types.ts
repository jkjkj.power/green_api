import { MessageType } from "./constants";

export type ApiParams = {
  idInstance: string;
  apiTokenInstance: string;
};

export type SendMessageReqBody = {
  chatId: string;
  message: string;
  quotedMessageId?: string;
};

export type SendMessageRes = {
  idMessage: string;
};

export type HistoryReqBody = {
  chatId: string;
  count: number;
};

export type HistoryRes = {
  type: MessageType;
  timestamp: Date;
  idMessage: string;
  typeMessage: string;
  chatId: string;
  senderId: string;
  senderName: string;
  textMessage: string;
};

export type ReceiveMessageRes = {
  receiptId: number;
  body: {
    typeWebhook: string;
    instanceData: {
      idInstance: number;
      wid: string;
      typeInstance: string;
    };
    idMessage: string;
    messageData: {
      typeMessage: string;
      textMessageData: {
        textMessage: string;
      };
    };
  };
};
