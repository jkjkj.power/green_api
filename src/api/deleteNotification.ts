import { Action } from "./constants";
import fetchRequest from "./fetchRequest";
import { ApiParams, ReceiveMessageRes } from "./types";

export async function deleteNotification(
  receiptId: number,
  { idInstance, apiTokenInstance }: ApiParams,
): Promise<ReceiveMessageRes | null> {
  try {
    return await fetchRequest<ReceiveMessageRes>(
      `${idInstance}/${Action.DELETE}/${apiTokenInstance}/${receiptId}`,
      {
        method: "DELETE",
      },
    );
  } catch (error) {
    console.error(error);

    return Promise.resolve(null);
  }
}
