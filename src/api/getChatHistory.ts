import { Action } from "./constants";
import fetchRequest from "./fetchRequest";
import { ApiParams, HistoryReqBody, HistoryRes } from "./types";

export async function getChatHistory(
  body: HistoryReqBody,
  { idInstance, apiTokenInstance }: ApiParams,
): Promise<HistoryRes[] | null> {
  try {
    return await fetchRequest<HistoryRes[]>(
      `${idInstance}/${Action.GET_HISTORY}/${apiTokenInstance}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      },
    );
  } catch (error) {
    console.error(error);

    return Promise.resolve(null);
  }
}
