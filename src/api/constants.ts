export enum Action {
  SEND = "sendMessage",
  RECEIVE = "receiveNotification",
  DELETE = "deleteNotification",
  GET_HISTORY = "getChatHistory",
}

export enum MessageType {
  INCOMING = "incoming",
  OUTGOING = "outgoing",
}

// добавлен один тип, так как не работаю с другими
export enum Webhook {
  INCOMING_MESSAGE_RECEIVED = "incomingMessageReceived",
}

export const CHAT_ID_POSTFIX = "@c.us";
