async function fetchRequest<T = null>(
  path: string,
  init?: RequestInit,
): Promise<T | null> {
  const serverUrl = "https://api.green-api.com/waInstance";

  const response = await fetch(serverUrl + path, init);

  if (!response.ok) {
    return Promise.reject(
      new Error(response.status + " (" + response.statusText + ")"),
    );
  }

  const method = init?.method?.toUpperCase();

  if (method === "DELETE" || method === "PATCH") {
    return Promise.resolve(null);
  }

  return (await response.json()) as Promise<T>;
}

export default fetchRequest;
