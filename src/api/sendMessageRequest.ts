import { Action } from "./constants";
import fetchRequest from "./fetchRequest";
import { ApiParams, SendMessageReqBody, SendMessageRes } from "./types";

export async function sendMessageRequest(
  body: SendMessageReqBody,
  { idInstance, apiTokenInstance }: ApiParams,
): Promise<SendMessageRes | null> {
  try {
    return await fetchRequest<SendMessageRes>(
      `${idInstance}/${Action.SEND}/${apiTokenInstance}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      },
    );
  } catch (error) {
    console.error(error);

    return Promise.resolve(null);
  }
}
