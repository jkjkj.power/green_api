import { Action } from "./constants";
import fetchRequest from "./fetchRequest";
import { ApiParams, ReceiveMessageRes } from "./types";

export async function receiveMessageRequest({
  idInstance,
  apiTokenInstance,
}: ApiParams): Promise<ReceiveMessageRes | null> {
  try {
    return await fetchRequest<ReceiveMessageRes>(
      `${idInstance}/${Action.RECEIVE}/${apiTokenInstance}`,
      {
        method: "GET",
      },
    );
  } catch (error) {
    console.error(error);

    return Promise.resolve(null);
  }
}
