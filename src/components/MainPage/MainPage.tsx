import { FC } from "react";
import { ApiParams } from "../../api/types";
import { useChat, useReceiveMessages } from "../hooks";
import { ChatList } from "../ui/ChatList";
import { Dialog } from "../ui/Dialog";
import { InputForm } from "../ui/InputForm";
import { MainPageStyled } from "./MainPage.styles";
import { Loading } from "../ui/Loading";

type Props = {
  apiParams: ApiParams;
  userPhone: string;
  currentChatPhone: string;
};

export const MainPage: FC<Props> = ({
  apiParams,
  userPhone,
  currentChatPhone,
}) => {
  const {
    messages,
    text,
    isLoading,
    setMessages,
    onInputChange,
    onSubmitMessage,
  } = useChat(userPhone, apiParams);

  useReceiveMessages(apiParams, setMessages);

  const lastMessageText =
    messages && messages[0] ? messages[0].textMessage : "";

  return (
    <MainPageStyled>
      <ChatList
        lastMessageText={lastMessageText}
        currentChatPhone={currentChatPhone}
      />
      <div>
        {isLoading ? <Loading /> : <Dialog messages={messages} />}
        <InputForm
          text={text}
          onChange={onInputChange}
          onSubmit={onSubmitMessage}
        />
      </div>
    </MainPageStyled>
  );
};
