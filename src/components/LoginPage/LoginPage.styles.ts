import { styled } from "styled-components";

export const LoginPageStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 15px;
  width: 900px;
  height: 400px;
  margin: auto;
`;
