import { FC } from "react";
import { InputStyled } from "../ui/InputForm";
import { LoginPageStyled } from "./LoginPage.styles";

type Props = {
  userData: {
    idInstance: string;
    apiTokenInstance: string;
    userPhone: string;
  };
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

export const LoginPage: FC<Props> = ({ userData, onChange }) => {
  return (
    <LoginPageStyled>
      <InputStyled
        withBorder
        placeholder="Введите idInstance"
        value={userData.idInstance}
        onChange={onChange}
        type="text"
        name="idInstance"
      />
      <InputStyled
        withBorder
        placeholder="Введите apiTokenInstance"
        value={userData.apiTokenInstance}
        onChange={onChange}
        type="text"
        name="apiTokenInstance"
      />
      <InputStyled
        withBorder
        placeholder="Введите номер телефона пользователя, с которым хотите создать чат, в формате 7ХХХХХХХХХХ"
        value={userData.userPhone}
        onChange={onChange}
        type="text"
        name="phone"
      />
    </LoginPageStyled>
  );
};
