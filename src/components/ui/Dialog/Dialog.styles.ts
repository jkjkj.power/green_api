import { styled } from "styled-components";

export const DialogStyled = styled.div`
  width: 70vw;
  border: 1px solid green;
  height: 92vh;
  background-color: antiquewhite;
  overflow-y: auto;
  transform: rotate(180deg);
  direction: rtl;
  padding-top: 5px;
`;
