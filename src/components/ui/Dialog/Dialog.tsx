import { FC } from "react";
import { MessageType } from "../../../api";
import { HistoryRes } from "../../../api/types";
import { Message } from "../Message";
import { DialogStyled } from "./Dialog.styles";

type Props = {
  messages: HistoryRes[];
};

export const Dialog: FC<Props> = ({ messages }) => {
  return (
    <DialogStyled>
      {messages.map((msg, index) => {
        return (
          <Message
            key={index}
            text={msg.textMessage}
            isSendedMessage={msg.type === MessageType.OUTGOING}
          />
        );
      })}
    </DialogStyled>
  );
};
