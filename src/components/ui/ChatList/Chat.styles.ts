import { styled } from "styled-components";

export const ChatIconStyled = styled.div`
  border: 1px solid green;
  border-radius: 100%;
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ChatContinerStyled = styled.div`
  display: flex;
  padding-top: 10px;
  cursor: pointer;

  &:hover {
    background-color: gainsboro;
  }
`;

export const ChatListContinerStyled = styled.div`
  overflow-y: auto;
  height: 99vh;
`;

export const ChatNameStyled = styled.div`
  color: black;
  font-weight: bold;
  font-size: 100%;
  width: 355px;
`;

export const ChatMsgPartStyled = styled.span`
  font-size: 14px;
`;

export const ChatInfoStyled = styled.div`
  padding: 0 5px;
  border-bottom: 1px solid gainsboro;
  width: 325px;
`;
