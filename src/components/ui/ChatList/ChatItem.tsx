import { FC } from "react";
import {
  ChatContinerStyled,
  ChatIconStyled,
  ChatInfoStyled,
  ChatMsgPartStyled,
  ChatNameStyled,
} from "./Chat.styles";

type Props = {
  phone: string;
  lastMessageText: string;
};

export const ChatItem: FC<Props> = ({ phone, lastMessageText }) => {
  return (
    <ChatContinerStyled>
      <ChatIconStyled>C</ChatIconStyled>
      <ChatInfoStyled>
        <ChatNameStyled>Чат с {phone}</ChatNameStyled>
        <ChatMsgPartStyled>{lastMessageText?.slice(0, 10)}...</ChatMsgPartStyled>
      </ChatInfoStyled>
    </ChatContinerStyled>
  );
};
