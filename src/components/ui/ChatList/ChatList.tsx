import { FC } from "react";
import { ChatListContinerStyled } from "./Chat.styles";
import { ChatItem } from "./ChatItem";

type Props = {
  currentChatPhone: string;
  lastMessageText: string;
};

export const ChatList: FC<Props> = ({ currentChatPhone, lastMessageText }) => {
  return (
    <ChatListContinerStyled>
      {[currentChatPhone].map((phone, index) => (
        <ChatItem key={index} phone={phone} lastMessageText={lastMessageText} />
      ))}
    </ChatListContinerStyled>
  );
};
