import { styled } from "styled-components";

export const InputContainerStyled = styled.div`
  height: 40px;
  background-color: #e9edef;
  padding: 10px;
`;

export const InputStyled = styled.input<{ withBorder?: boolean }>`
  border-radius: 6px;
  width: 99%;
  border: ${(props: { withBorder?: boolean }) =>
    props.withBorder ? "1px solid green" : "none"};
  height: 40px;
  padding: 0 5px;
  font-size: 16px;
`;
