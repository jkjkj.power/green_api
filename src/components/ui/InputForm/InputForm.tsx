import { FC } from "react";
import { InputContainerStyled, InputStyled } from "./Input.styles";

type Props = {
  text: string;
  onChange: (e: React.ChangeEvent) => void;
  onSubmit: (event: React.FormEvent) => void;
};
export const InputForm: FC<Props> = ({ text, onChange, onSubmit }) => {
  return (
    <InputContainerStyled>
      <form onSubmit={onSubmit}>
        <InputStyled
          value={text}
          onChange={onChange}
          placeholder="Введите сообщение"
        />
      </form>
    </InputContainerStyled>
  );
};
