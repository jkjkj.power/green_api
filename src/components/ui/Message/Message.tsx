import { FC } from "react";
import { MessageContainerStyled, MessageStyled } from "./Message.styles";

type Props = {
  text: string;
  isSendedMessage?: boolean;
};

export const Message: FC<Props> = ({ isSendedMessage, text }) => (
  <MessageContainerStyled isSendedMessage={isSendedMessage}>
    <MessageStyled isSendedMessage={isSendedMessage}>{text}</MessageStyled>
  </MessageContainerStyled>
);
