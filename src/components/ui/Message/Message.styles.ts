import { styled } from "styled-components";

export const MessageStyled = styled.div<{ isSendedMessage?: boolean }>`
  max-width: 550px;
  height: fit-content;
  background-color: ${({ isSendedMessage }: { isSendedMessage?: boolean }) =>
    isSendedMessage ? "#d9fdd3" : "#FFFFFF"};
  border-radius: 8px;
  padding: 8px;
  margin: 2px 0;
  transform: rotate(180deg);
  direction: ltr;
`;

export const MessageContainerStyled = styled.div<{ isSendedMessage?: boolean }>`
  display: flex;
  justify-content: ${({ isSendedMessage }: { isSendedMessage?: boolean }) =>
    isSendedMessage ? "flex-end" : "flex-start"};
  padding: 0 60px;
`;
