import { styled } from "styled-components";

export const LoadingStyled = styled.div`
  width: 70vw;
  height: 93vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;
