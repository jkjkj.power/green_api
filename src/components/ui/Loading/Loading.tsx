import { LoadingStyled } from "./Loading.styles";

export const Loading = ({ text }: { text?: string }) => {
  return <LoadingStyled>{text ?? "Loading.."}</LoadingStyled>;
};
