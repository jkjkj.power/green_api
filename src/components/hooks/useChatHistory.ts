import { useState, useEffect } from "react";
import { CHAT_ID_POSTFIX, getChatHistory } from "../../api";
import { ApiParams, HistoryRes } from "../../api/types";

export const useChatHistory = (
  userPhone: string,
  apiParams: ApiParams,
  count?: number
) => {
  const [messages, setMessages] = useState<HistoryRes[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const body = {
      chatId: userPhone + CHAT_ID_POSTFIX,
      count: count ?? 30,
    };

    (async () => {
      try {
        setIsLoading(true);
        const data = await getChatHistory(body, apiParams);
        setMessages(data);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    })();
  }, []);

  return { messages, setMessages, isLoading } as const;
};
