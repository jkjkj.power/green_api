import * as React from "react";
import { useEffect, Dispatch } from "react";
import {
  deleteNotification,
  MessageType,
  receiveMessageRequest,
  Webhook,
} from "../../api";
import { ApiParams, HistoryRes } from "../../api/types";

export const useReceiveMessages = (
  apiParams: ApiParams,
  setMessagesFn: Dispatch<React.SetStateAction<HistoryRes[]>>
) => {
  useEffect(() => {
    let intervalId: NodeJS.Timeout;

    try {
      intervalId = setInterval(async () => {
        const data = await receiveMessageRequest(apiParams);

        if (!data) return;

        const textMessage = data.body.messageData?.textMessageData?.textMessage;

        await deleteNotification(data.receiptId, apiParams);

        if (data.body.typeWebhook === Webhook.INCOMING_MESSAGE_RECEIVED) {
          setMessagesFn((prev) => {
            const updatedMessages = [
              {
                textMessage,
                idMessage: data.body.idMessage,
                type: MessageType.INCOMING,
              },
              ...prev,
            ];

            return [
              ...Array.from(
                new Map(
                  updatedMessages.map((item) => [item.idMessage, item])
                ).values()
              ),
            ] as HistoryRes[];
          });
        }
      }, 1000);
    } catch (err) {
      console.error(err);
    }

    return () => clearInterval(intervalId);
  }, []);
};
