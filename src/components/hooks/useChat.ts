import { useState } from "react";
import { CHAT_ID_POSTFIX, MessageType, sendMessageRequest } from "../../api";
import { HistoryRes, ApiParams } from "../../api/types";
import { useChatHistory } from ".";

export const useChat = (userPhone: string, apiParams: ApiParams) => {
  const [text, setText] = useState("");
  const { messages, setMessages, isLoading } = useChatHistory(
    userPhone,
    apiParams
  );

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const onSubmitMessage = async (event: React.FormEvent) => {
    event.preventDefault();

    const body = {
      chatId: userPhone + CHAT_ID_POSTFIX,
      message: text,
    };

    setText("");
    setMessages((prev) => [
      {
        textMessage: text,
        type: MessageType.OUTGOING,
        idMessage: messages.length.toString(),
      } as HistoryRes,
      ...prev,
    ]);
    try {
      await sendMessageRequest(body, apiParams);
    } catch (err) {
      console.error(err);
    }
  };

  return {
    messages,
    text,
    isLoading,
    setMessages,
    onInputChange,
    onSubmitMessage,
  };
};
