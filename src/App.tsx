import { useState } from "react";
import { ApiParams } from "./api/types";
import { MainPage } from "./components";
import { LoginPage } from "./components/LoginPage/LoginPage";

const App = () => {
  const [apiParams, setApiParams] = useState<ApiParams>({} as ApiParams);
  const [userPhone, setUserPhone] = useState("");

  const onLoginInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    if (name === "phone") {
      setUserPhone(value);
    } else {
      setApiParams((prev) => ({ ...prev, [name]: value } as ApiParams));
    }
  };

  const isUserLogged = () => {
    return (
      !!apiParams?.apiTokenInstance &&
      !!apiParams?.idInstance &&
      userPhone.length === 11
    );
  };

  return isUserLogged() ? (
    <MainPage
      apiParams={apiParams}
      userPhone={userPhone}
      currentChatPhone={userPhone}
    />
  ) : (
    <LoginPage
      userData={{ ...apiParams, userPhone }}
      onChange={onLoginInputChange}
    />
  );
};

export default App;
